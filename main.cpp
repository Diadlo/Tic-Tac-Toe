#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <time.h>
#include <stdlib.h>

using namespace std;

const int X = 1; O = -1;

int game = 1, sum, StepNum = 0, t, px, py, clear = -1;
int a[4][4] = {{0}};

void gameX(), gameO();
void p_loc();
void PlayerStep(int);
void OCenter(), OCorner(), OSide();
int CheckWin(int);
int Step(int), Game(int);

int main()
{
    srand(time(NULL));
    char  ch = 0;
    int type;
    char c;
	while (clear == -1)
    {
		printf("Clear screen after step? (y/n)? \n");
		cin >> c;
		if (c == 'y' || c == 'Y' || c == 1) {clear = 1;}
		else if (c == 'n' || c == 'N' || c == 0) {clear = 0; }
		else printf("Wrong symbol\nTry again:");
	}
	cout << "For select position use NumPad" << endl 
											<< "789" << endl 
											<< "456" << endl 
											<< "123" << endl; 
    printf("Choose your fighter (X or O)! \n");
    cin >> ch;
	if (ch == 'o' || ch == 'O' || ch == '0' || ch == '\320') {cin.clear(); cin.ignore(100000, '\n'); type = O;}
	if (ch == 'x' || ch == 'X') {cin.clear(); cin.ignore(100000, '\n'); type = X;}
	while (game)
	{
		StepNum = 0;
		if (clear)
			if (system("cls")) system("clear");
		switch (type)                 //Choose game type
		{
		case O:{gameX(); break;}
		case X:{gameO(); break;}
		default:{printf("Bad symbol. Game Over \n"); break;}
		}
		cout << "Enter 0 if you want to go to any other value, if we want continue" << endl;
	    cin >> game;
		for (int i = 2; i >= 0; i--){
				for (int j = 0; j < 3; j++){
					a[i][j] = 0;}}
	}
    return 0;
}


//////////////////////////////////////////////////

void gameX()
{
    p_loc();
    int r = Step(X);
    StepNum++;
    switch (r)
    {
    case 0:{a[1][1] = 1; break;}
    case 1:{  //diagonal
        for (int i = 0; i < 3; i++)
            if (a[i][2-i] == 0) a[i][2-i] = 1; break;}
    case 2:{  //row
        for(int i = 0; i < 3; i++)
            if (a[t][i] == 0) {a[t][i] = 1;} break;}
    case 3:{  //column
        for(int i = 0; i < 3; i++)
            if (a[i][t] == 0) {a[i][t] = 1;} break;}
    case 4:{  //corner
        if (px == 1) px = px + 1 + (rand() % 2) * (-2);
        if (py == 1) py = py + 1 + (rand() % 2) * (-2);
        if (a[2-px][2-py] == 0)    //most remout corner
            {a[2-px][2-py] = X; break;}
        if (rand() % 2)   //OR
            if (a[2-px][py] == 0) {a[2-px][py] = X; break;} //on of last 2 corner
        if (a[px][2-py] == 0) {a[px][2-py] = X; break;} //on of last 2 corner

        if (rand() % 2)   //OR
            if (a[px][py] == 0) {a[px][py] = X; break;} //on of last 2 corner
        if (a[2-px][2-py] == 0) {a[2-px][2-py] = X; break;} //on of last 2 corner
        for (int i = 0; i < 2; i++)
        {
            if (r == 4 && a[i][1-i] == 0)
                {a[i][1-i] = X; break;}
            if (r == 4 && a[1+i][2-i] == 0)
                {a[1+i][2-i] = X; break;}
        }
        break;}
    case 5:{return; break;}
    case 6:{
        for(int i = 0; i < 3; i++)
            if (a[i][i] == 0) {a[i][i] = X;} break;}
    }
    if (CheckWin(X)) return;
    printf("Computer step:\n");
    p_loc();
    PlayerStep(O);
    StepNum++;
    gameX();
    return;
}

void gameO()
{
    p_loc();
    PlayerStep(X);
    StepNum++;
    if (px == 1 && py == 1) {OCenter(); return;}
    if (px+py == 2 || px+py == 0 || px+py == 4) {OCorner(); return;}
    if (px+py == 1 || px+py == 3) {OSide(); return;}
}

void OCenter()
{
    p_loc();
    int r = Step(O);
    switch(r){
    case 1:{  //diagonalX
        for (int i = 0; i < 3; i++)
            if (a[i][2-i] == 0) a[i][2-i] = O; break;}
    case 2:{  //row
        for(int i = 0; i < 3; i++)
            if (a[t][i] == 0) {a[t][i] = O;} break;}
    case 3:{  //column
        for(int i = 0; i < 3; i++)
            if (a[i][t] == 0) {a[i][t] = O;} break;}
    case 4:{  //corner
        while (r == 4)
        {
            for (int i = 0; i < 3; i += 2) //corner
            {
                if (r && a[i][i] == 0)
                    {a[i][i] = O; r = 0;}
                if (r && a[0][2] == 0)
                    {a[i][2-i] = O; r = 0;}
            }
            for (int i = 0; i < 2; i++)  //side
            {
                if (r && a[i][1+i] == 0)
                    {a[i][i+1] = O; r = 0;}
                if (r && a[i+1][i] == 0)
                    {a[i+1][i] = O; r = 0;}
            }
        }
        break;
    }
    case 5:{return; break;}
    case 6:{
        for(int i = 0; i < 3; i++)
            if (a[i][i] == 0) {a[i][i] = O;} break;}
    }
    if (CheckWin(X)) return;
    printf("Computer step:\n");
    p_loc();
    StepNum++;
    PlayerStep(1);
    StepNum++;
    OCenter();
    return;
}

void OCorner()
{
    p_loc();
    int r = Step(O);
    switch(r){
    case 0:{ a[1][1] = O; break;}
    case 1:{  //diagonalX
        for (int i = 0; i < 3; i++)
            if (a[i][2-i] == 0) a[i][2-i] = O; break;}
    case 2:{  //row
        for(int i = 0; i < 3; i++)
            if (a[t][i] == 0) {a[t][i] = O;} break;}
    case 3:{  //column
        for(int i = 0; i < 3; i++)
            if (a[i][t] == 0) {a[i][t] = O;} break;}
    case 4:{  //corner
        if (px == 1) px = px + 1 + (rand() % 2) * (-2);
        if (py == 1) py = py + 1 + (rand() % 2) * (-2);
        if (a[2-px][2-py] == 0)    //most remout corner
            {a[2-px][2-py] = O; break;}
        for (int i = 0; i < 2; i++)
        {
            if (r == 4 && a[i][1-i] == 0)
                {a[i][1-i] = O; break;}
            if (r == 4 && a[1+i][2-i] == 0)
                {a[1+i][2-i] = O; break;}
        }
        break;
    }
    case 5:{return; break;}
    case 6:{
        for(int i = 0; i < 3; i++)
            if (a[i][i] == 0) {a[i][i] = O;} break;}
    }
    if (CheckWin(X)) return;
    printf("Computer step:\n");
    p_loc();
    StepNum++;
    PlayerStep(X);
    StepNum++;
    OCorner();
    return;
}
void OSide()
{
    p_loc();
    int r = Step(O);
    switch(r){
    case 0:{ a[1][1] = O; break;}
    case 1:{  //diagonalX
        for (int i = 0; i < 3; i++)
            if (a[i][2-i] == 0) a[i][2-i] = O; break;}
    case 2:{  //row
        for(int i = 0; i < 3; i++)
            if (a[t][i] == 0) {a[t][i] = O;} break;}
    case 3:{  //column
        for(int i = 0; i < 3; i++)
            if (a[i][t] == 0) {a[i][t] = O;} break;}
    case 4:{  //corner
		int ok = 0;
		if (StepNum == 3)
		{
			for (int i = 0; i < 2; i++){
				if (a[0+i][1+i] == a[1+i][0+i] && a[1+i][0+i] == X) {a[i*2][i*2] = O; ok = 1; break;}
				if (a[1-i][0+i] == a[2-i][1+i] && a[2-i][1+i] == X) {a[2+i*(-2)][i*2] = O; ok = 1; break;}
				}
		}
		while (ok == 0)
		{
			int rand_x = rand() % 3, rand_y = rand() % 3;
			if (a[rand_x][rand_y] == 0) {a[rand_x][rand_y] = O; ok = 1;}
		}
		break;
    }
    case 5:{return; break;}
    case 6:{
        for(int i = 0; i < 3; i++)
            if (a[i][i] == 0) {a[i][i] = O;} break;}
    }
    if (CheckWin(X)) return;
    printf("Computer step:\n");
    p_loc();
    StepNum++;
    PlayerStep(X);
    StepNum++;
    OSide();
    return;
}
void p_loc() //print location
{
	if (clear)
		if (system("cls")) system("clear");
    for (int i = 2; i >= 0; i--)
    {
        for (int j = 0; j < 3; j++)
        {
            if (a[i][j] == 0 ) printf("-");
            if (a[i][j] == O ) printf("O");
            if (a[i][j] == X ) printf("X");
        }
        printf("\n");
    }
    return;
}

int Step(int F)
{
    if (a[1][1] == 0) return 0; //symbol in center

    if (CheckWin(O)) return 5;

    if (a[3][3] == 2*F) {return 6;}
    if (sum == 2*F) {t = 2; return 1;}
    for (int i = 0; i < 4; i++) // BEGIN check for 2 contact symbol
    {
        if (a[3][i] == 2*F) {t = i; return 3;}
        if (a[i][3] == 2*F) {t = i; return 2;}
    }                           //END check for 2 contact symbol

    if (a[3][3] == 2*(-F)) {return 6;}
    if (sum == 2*(-F)) {t = 2; return 1;}
    for (int i = 0; i < 4; i++) // BEGIN check for 2 contact symbol
    {
        if (a[3][i] == 2*(-F)) {t = i; return 3;}
        if (a[i][3] == 2*(-F)) {t = i; return 2;}
    }
    return 4;
}

void account()  //account sum on row and corner
{
    sum = 0;
    for (int i = 0; i < 4; i++)
    {
        a[3][i] = 0;
        a[i][3] = 0;
    }
    for (int i = 0; i < 3; i++)
    {
        a[3][3] = a[3][3] + a[i][i];
        sum = sum + a[i][2-i];
        for (int j = 0; j < 3; j++)
        {
            a[i][3] = a[i][3] + a[i][j];
            a[3][i] = a[3][i] + a[j][i];
        }
    }
}

int CheckWin(int fighter)  //check win or lose
{
    void account();

    account();
    if (StepNum == 9)
    {
        printf("Computer step:\n");
        p_loc();
        printf("This is draw \n");
        return 1;
    }
    for (int i = 0; i < 4; i++)
    {
        if ((a[3][i] == 3*O) || (a[i][3] == 3*O) || (sum == 3*O))
        {
            printf("Computer step:\n");
            p_loc();
            if (fighter == O)
                printf("You win \n");
            else
                printf("You lose \n");
            return 1;
        }
        if ((a[3][i] == 3*X) || (a[i][3] == 3*X) || (sum == 3*X))
        {
            printf("Computer step:\n");
            p_loc();
            if (fighter == X)
                printf("You lose \n");
            else
                printf("You win \n");
            return 1;
        }
    }
    return 0;
}

void PlayerStep(int fighter)
{
    printf("Enter position: ");
    t = 0;
    while (t == 0)
    {
        int q;
        cin >> q;
        px = (q-1) /3;
        py = (q-1) %3;
        if (!(q < 10 && q > 0)) {cin.clear(); cin.ignore(100000, '\n'); printf("Wrong position\nTry again: ");}
        else
            if (fighter == O)
            {
                if (a[px][py] == 0) {a[px][py] = O; t = 1;}
                    else printf("This position occupied \nTry again: ");
            }
            else
            {
                if (a[px][py] == 0) {a[px][py] = X; t = 1;}
                    else printf("This position occupied \nTry again: ");
            }
    }
}
